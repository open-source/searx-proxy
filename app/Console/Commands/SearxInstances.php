<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SearxInstances extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instances:load';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'loads a list of running searx instances ( https://searx.space )';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://searx.space/data/instances.json');
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);
        if($response !== null){
            $instances = [];
            foreach($response->instances as $instance => $data) {
                if(!isset($data->{'error'}) &&
                $data->{'network_type'} !== 'tor' &&
                isset($data->{'timing'}->{'search_go'}) &&
                isset($data->{'timing'}->{'search_go'}->{'all'}) &&
                isset($data->{'timing'}->{'search_go'}->{'all'}->{'median'}) &&
                $data->{'timing'}->{'search_go'}->{'all'}->{'median'} <= 1.8) {
                    try{
                        $opensearch = file_get_contents($instance . 'opensearch.xml');
                        if(!str_contains($opensearch, 'SyndicationRight')) {
                            $instances[] = $instance;
                        }
                    }catch(\Exception $e) {
                        
                    }
                }
            }
            $f = fopen('storage/instances.txt', 'w');
            fwrite($f, implode("\n", $instances));
            fclose($f);
        } else {
            echo "Unable to locate file. Check https://searx.space/data/instances.json for more info\n";
        }
    }
}