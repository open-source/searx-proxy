<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SearxProxy extends Controller
{
    public function search(Request $request, $retry = 0) {
        $search = $request->input('search');

        $instances = file('/html/storage/instances.txt');
        $host = trim($instances[rand(0,(sizeof($instances)-1))]);
        $url = $host . '?q=' . $search . '&format=json';
        try{
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            $response = curl_exec($ch);
            curl_close($ch);

            if(json_decode($response) !== null){
                return response()->json(['hostname' => $host, 'results' => json_decode($response)]);
            }else {
                throw new \Exception('invalid response');
            }
        }catch(\Exception $e) {
            if ($retry < 3) {
                $this->search($request, $retry+1);
            }
        }
    }
}
