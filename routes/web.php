<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SearxProxy;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [SearxProxy::class, 'search']);

Route::get('healthz', function () {
    return response('', 200)
        ->header('Content-Type', 'text/plain');
});